---
title: About
published: 2015-12-13
comments: off
toc: off
---

<img src="/images/crimson_king.jpg" class="right" width="256" id="me">

My name is Roberto Di Remigio.
I am a PhD student in [theoretical chemistry] at the University of Tromsø, Norway.
My current research project is on [continuum solvation models].
I am a developer of [PCMSolver], an application programming interface offering
the [polarizable continuum model] functionality to quantum chemistry codes.
I contribute or have contributed to the development of popular quantum chemistry codes:
[DIRAC], [DALTON], [LSDALTON], [Psi4] and [ReSpect].

[theoretical chemistry]: https://en.wikipedia.org/wiki/Theoretical_chemistry
[continuum solvation models]: https://en.wikipedia.org/wiki/Implicit_solvation
[PCMSolver]: http://pcmsolver.readthedocs.org/
[polarizable continuum model]: https://en.wikipedia.org/wiki/Polarizable_continuum_model
[DIRAC]: http://diracprogram.org
[DALTON]: http://www.daltonprogram.org/
[LSDALTON]: http://www.daltonprogram.org/
[Psi4]: http://www.psicode.org/
[ReSpect]: http://rel-qchem.sav.sk/

I am interested in programming. I usually program in [C++] and [Fortran].
I am trying to bring as much [Python] as possible into the mix.
I have lately discovered [functional programming] and [Haskell]. I am still learning,
but I see a lot of potential in it.

[C++]: https://en.wikipedia.org/wiki/C%2B%2B
[Fortran]: https://en.wikipedia.org/wiki/Fortran
[Python]: https://en.wikipedia.org/wiki/Python_(programming_language)
[functional programming]: https://en.wikipedia.org/wiki/Functional_programming
[Haskell]: http://en.wikipedia.org/wiki/Haskell_(programming_language)

I love to [read] and not only about technical subjects. I try to write [notes]
about what I read as often as possible, mostly to help me not to forget!

[read]: /reads/
[notes]: /notes/

Find me on [GitHub] and [GitLab], where much of my programming happens.
I try to keep an up-to-date list of my [current] activities.
Email me at **roberto.diremigio@gmail.com** or find me on [twitter].

[current]: /currently
[GitHub]: http://github.com/robertodr
[GitLab]: https://gitlab.com/u/robertodr
[twitter]: https://twitter.com/_boberto_
