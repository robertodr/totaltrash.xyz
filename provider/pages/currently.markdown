---
title: Currently
published: 2015-12-13
comments: off
toc: off
---

My current activities. Possibly not up-to-date.

## Reading

I am used to read more than one book at a time
and this is what's currently on my bedside table.

<img src="/images/books/nakedlunch.jpg" class="right" width="128">

* [Naked Lunch](http://amzn.com/0802132952) --- This book has been on my to-read list
  for quite some time. I found a paperback copy in a thrift store and that's when
  I finally started reading it. [Burroughs] was part of the Beat movement and has had
  a great influence on the later No Wave movement.

    *Naked Lunch* is a challenging book to read. Burroughs uses a nonlinear
    style, result of the cut-up technique. According to him, any point in the
    book is a good place where to start reading.

[Burroughs]: https://en.wikipedia.org/wiki/William_S._Burroughs

<img src="/images/books/hutton.jpg" class="right" width="128">

* [Programming in Haskell](http://amzn.com/0521692695) --- I learnt what little
  I know about programming in C++ mainly to solve the problems related to my
  Ph.D. project. Haskell and functional programming are something I have become
  curious about lately. There is a lot of potential in learning a different
  way of thinking when programming and the strong connection of Haskell with
  mathematics made me very curious.

    This is the textbook for the [FP101x] course offered on edX.
    The book is clear: you don't need any previous experience with Haskell
    or knowledge of category theory.

[FP101x]: http://bit.ly/1N321WD

<img src="/images/books/omhosten.jpg" class="right" width="128">

* [Om Høsten](http://bit.ly/1P1DAYb) --- I am trying to learn Norwegian and I
  have wanted for a while to read something challenging. It is a slow process:
  I need to look up words on a dictionary quite often. It is very rewarding though!

    My first encounter with [Karl Ove Knausgård] was actually in English
    translation on the Fiction page of [The New Yorker] magazine. It was an
    excerpt from the third volume of his [six-volume autobiography]. I was
    intrigued by this project and decided to read it in Norwegian. Needless to
    say, I did not progress much further than the first few pages of the first
    volume. I lowered my ambitions a notch and decided to start with this book of his insstead.
    *Om Høsten* is part of a series of four. Each book is a collection of
    short letters to his unborn daughter, describing and explaining what things she
    will meet in the world.

[Karl Ove Knausgård]: https://en.wikipedia.org/wiki/Karl_Ove_Knausg%C3%A5rd
[The New Yorker]: http://www.newyorker.com/magazine/2014/02/17/come-together-3
[six-volume autobiography]: https://en.wikipedia.org/wiki/My_Struggle_(Knausg%C3%A5rd_novels)

## Working On

